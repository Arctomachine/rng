<?php
require_once('./dbconnect.php');

$sql = 'CREATE DATABASE IF NOT EXISTS random_numbers';
if ($conn->query($sql)) {
	echo '<br />' . 'Database created';
	$sql2 = 'CREATE TABLE IF NOT EXISTS `random_numbers`.`numbers` ( `id` INT NOT NULL AUTO_INCREMENT , `number` INT NOT NULL , `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;';
	if ($conn->query($sql2)) {
		echo '<br />' . 'Table created';
	} else {
		echo '<br />' . $conn->error;
	}
} else {
	echo '<br />' . $conn->error;
}
$conn->close();
