<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</head>

<body>

    <nav>
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">RNG API</a>
        </div>
    </nav>
    <main>
        <div class="container">
            <h2>Getting started</h2>
            <p>Simple random number generator (RNG) API. Saves generated results for future access.</p>
            <div class="divider"></div>

            <h3>Generate number</h3>
            <p>
                <a href="/api/generate">/api/generate</a> generates new random number.
            </p>
            <div class="divider"></div>

            <h3>Access old result</h3>
            <p><a href="/api/retrieve/5">/api/retrieve/id</a> returns previously generated number where `id` is its assigned id.</p>
            <div class="divider"></div>

            <h3>Data format</h3>
            <p> Both endpoints will return json file of following format: {
                "id": 5,
                "number": 1790397563,
                "createdAt": "2022-01-20 20:20:41"
                }
            </p>
            <ul class="collection">
                <li class="collection-item">
                    <b>number</b><br>
                    Random number <br>
                    Field type: number
                </li>
                <li class="collection-item">
                    <b>id</b> <br>
                    Internal id to access number in future <br>
                    Field type: number
                </li>
                <li class="collection-item">
                    <b>createdAt</b> <br>
                    Date and time of generation. <br>
                    Format: YYYY-MM-DD HH:MM:SS <br>
                    Field type: string
                </li>
            </ul>
        </div>
    </main>
    <footer class="page-footer">
        <div class="container">
            <div class="center">2022</div>
        </div>
    </footer>

</body>

</html>