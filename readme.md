## Requirements

Developed and tested:

-   PHP 7.4
-   MariaDB 10.4
-   Apache 2.4
-   Nginx 1.17

## Installation

1. Inside `utils/dbconnect.php` fill `$host`, `$username`, `$password`;
2. Run `utils/setup.php` once;
3. Inside `utils/.htaccess` uncomment `#Deny from all` (remove `#`).

## Usage

-   `/api/generate` generates new random number;
-   `/api/retrieve/id` returns previously generated number where `id` is its assigned id.

Both endpoints will return json file of following format:

-   `number`: random number;
-   `id`: internal id to access number in future;
-   `createdAt`: date and time of generation.

## Code format style

Formatted according to PSR-12.
