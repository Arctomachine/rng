<?php

// process incoming request to /api/generate and /api/retrieve/id
// generate random number, save to database, return number, id and date of creation
// get number by provided id from database

header('Content-Type:application/json');
$json = []; // array for future response

if ($_REQUEST['method']) {
    if ($_REQUEST['method'] == 'generate') { // /api/generate
        $json['number'] = rand(); // random number
        require('./utils/dbconnect.php');
        $str = "INSERT INTO random_numbers.numbers (number) VALUES (" . $json['number'] . ")";
        if ($conn->query($str)) {
            $id = $conn->insert_id; // get id of new record if inserted successfully
            $json = fetchData($id, $json);
        } else { // failed to insert
            http_response_code(500); // internal server error
            $json['status'] = 500;
            $json['message'] = $conn->error;
        }
        http_response_code(200); // OK
    } elseif ($_REQUEST['method'] == 'retrieve') {
        if ($_REQUEST['id'] and preg_match('/^[0-9]+$/', $_REQUEST['id'])) { // /api/retrieve/123
            $id = (int)$_REQUEST['id'];
            $json = fetchData($id, $json);
        } else { // id is not number
            http_response_code(400); // bad request
            $json['status'] = 400;
            $json['message'] = 'Invalid id, must be numerical value';
        }
    } else { // unknown method or empty
        http_response_code(400); // bad request
        $json['status'] = 400;
        $json['message'] = 'Invalid method';
    }
} else { // unknown method or empty
    http_response_code(400); // bad request
    $json['status'] = 400;
    $json['message'] = 'Invalid method';
}

echo json_encode($json);

// get record from database by id and put values into json
function fetchData($id, $json) // -> array
{
    require('./utils/dbconnect.php');
    $str = "SELECT * FROM random_numbers.numbers WHERE id = $id";
    $result = $conn->query($str);
    if ($result->num_rows > 0) { // if something came from database
        foreach ($result as $row) { // add new fields into array
            $json['id'] = (int)$row['id'];
            $json['number'] = (int)$row['number'];
            $json['createdAt'] = $row['createdAt'];
            break; // stop after first item in case there were two items with same id
        }
    } else {
        http_response_code(400); // bad request
        $json['status'] = 400;
        $json['message'] = 'Id not found';
    }
    mysqli_close($conn);
    return $json; // return new value, not mutate original
}
